import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { color } from '../../utils/colors'
import { fonts } from '../../utils/fonts'
import { IcInformasiKesehatan, IcPemeriksaan, IcRiwayatKesehatan, IcRiwayatObat, IcSuratRujukan } from '../../assets/icon'
import Gap from '../atoms/Gap'

const CardPemeriksaan = ({onPress, title, desc, status, no_urut, ...restProps}) => {

    const Icon = () => {
        switch(title){
            case 'Pemeriksaan':
                return <IcPemeriksaan />

            case 'Riwayat Kesehatan':
                return <IcRiwayatKesehatan />

            case 'Informasi Kesehatan':
                    return <IcInformasiKesehatan />

            case 'Riwayat Obat':
                return <IcRiwayatObat />

            case 'Surat Rujukan':
                return <IcSuratRujukan />

            default:
                return <IcPemeriksaan />
        }
    }

  return (
    <TouchableOpacity onPress={onPress} style={styles.container} {...restProps}>
      <View style={{flexDirection: 'row'}}>
        <View style={{justifyContent: 'center',}}>
            <Icon />
        </View>
        
        <View style={{marginLeft: 9, flexDirection: 'row',justifyContent: 'space-between'}}>
            <View>
                <Text style={styles.title}>{title}</Text>
                <Gap height={3} />
                <Text style={styles.desc}>{desc}</Text>
                <Gap height={3} />
                <Text style={styles.desc}>{no_urut}</Text>
                <Gap height={3} />
            </View>

            <View style={{
                maxWidth: 50
            }}>
                 <Text style={styles.desc}>{status}</Text>
            </View>

            
        </View>
      </View>

    </TouchableOpacity>
  )
}

export default CardPemeriksaan

const styles = StyleSheet.create({
    container: {
        backgroundColor: color.white,
        borderRadius: 10,
        paddingVertical: 20
    },
    title:{
        color: 'black',
        fontSize: 13
    },
    desc:{
        color: '#9F9F9F',
        fontSize: 11,
        width: 250
    }
})