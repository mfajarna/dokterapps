import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import Gap from '../atoms/Gap'

const CardSuratRujukan = ({onPress, no_surat,tanggal}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text>Klik untuk lihat surat rujukan</Text>
      <Gap height={20} />
      <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 20
      }}>
        <Text style={{
            marginRight: 20
        }}>No Surat</Text>
        <Text style={{
            maxWidth: 200
        }}>{no_surat}</Text>
      </View>
      <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 20
      }}>
        <Text style={{
            marginRight: 20
        }}>Tanggal</Text>
        <Text style={{
            maxWidth: 200
        }}>{tanggal}</Text>
      </View>
      
    </TouchableOpacity>
  )
}

export default CardSuratRujukan

const styles = StyleSheet.create({
    container:{
        marginBottom: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        backgroundColor: 'white',
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 15
    }
})