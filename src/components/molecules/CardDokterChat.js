import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import Gap from '../atoms/Gap'

const CardDokterChat = ({namaDokter,onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={{
          fontSize: 17,
          fontWeight: 'bold'
      }}>{namaDokter}</Text>
      <Gap height={5} />
      <Text style={{
          fontSize: 14,
          fontWeight: '300'
      }}>Klik untuk memberikan pesan ke pasien!</Text>
    </TouchableOpacity>
  )
}

export default CardDokterChat

const styles = StyleSheet.create({
    container:{
        marginBottom: 20,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,

        elevation: 16,
        paddingHorizontal: 15,
        paddingVertical: 20,
        borderRadius: 20
    }
})