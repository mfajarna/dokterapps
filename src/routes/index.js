import  React from 'react';

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack"

import { NavigationContainer } from '@react-navigation/native';

import SplashScreen from '../pages/SplashScreen';
import Loginscreen from '../pages/LoginScreen';

import HomeScreen from '../pages/HomeScreen';
import Profile from '../pages/Profile';
import BottomNavigator from '../components/molecules/BottomNavigator';
import MessageScreen from '../pages/MessageScreen';
import MyProfileScreen from '../pages/MyProfileScreen';

import PemeriksaanScreen from '../pages/PemeriksaanScreen';
import DaftarPemeriksaanScreen from '../pages/DaftarPemeriksaanScreen';
import RiwayatKesehatanScreen from '../pages/RiwayatKesehatanScreen';
import RiwayatObatScreen from '../pages/RiwayatObatScreen';
import SuratRujukanScreen from '../pages/SuratRujukanScreen';
import LihatSuratRujukan from '../pages/LihatSuratRujukan';
import DetailRiwayatKesehatan from '../pages/DetailRiwayatKesehatan';
import DetailRiwayatObatScreen from '../pages/DetailRiwayatObatScreen';
import DetailSuratRujukanScreen from '../pages/DetailSuratRujukanScreen';
import ChattingScreen from '../pages/ChattingScreen';
import ChangePassword from '../pages/ChangePassword';
import InformasiKesehatan from '../pages/InformasiKesehatan';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return(
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen
                options={{headerShown: false}}
                name="Home"
                component={HomeScreen}
            
            />

            <Tab.Screen
                options={{headerShown: false}}
                name="Message"
                component={MessageScreen}
            
            />

            <Tab.Screen
                options={{headerShown: false}}
                name="Profile"
                component={Profile}
            
            />
            
        </Tab.Navigator>
    )
}
 
const Routes = () => {
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName='SplashScreen'>

                <Stack.Screen
                    name = "MainApp" 
                    component={MainApp}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name="SplashScreen"
                    component={SplashScreen}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name="LoginScreen"
                    component={Loginscreen}
                    options={{ headerShown: false}}
                />
    

                <Stack.Screen
                    name = "HomeScreen" 
                    component={HomeScreen}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name = "Profile" 
                    component={Profile}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name = "MessageScreen" 
                    component={MessageScreen}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name = "MyProfileScreen" 
                    component={MyProfileScreen}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name = "PemeriksaanScreen" 
                    component={PemeriksaanScreen}
                    options={{ headerShown: false}}
                />
                <Stack.Screen
                    name = "RiwayatKesehatanScreen" 
                    component={RiwayatKesehatanScreen}
                    options={{ headerShown: false}}
                />
                
                <Stack.Screen
                    name = "DaftarPemeriksaanScreen" 
                    component={DaftarPemeriksaanScreen}
                    options={{ headerShown: false}}
                />

                                
                <Stack.Screen
                    name = "RiwayatObatScreen" 
                    component={RiwayatObatScreen}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name = "SuratRujukanScreen" 
                    component={SuratRujukanScreen}
                    options={{ headerShown: false}}
                />  
                <Stack.Screen
                    name = "LihatSuratRujukan" 
                    component={LihatSuratRujukan}
                    options={{ headerShown: false}}
                />  
                <Stack.Screen
                    name = "DetailRiwayatKesehatan" 
                    component={DetailRiwayatKesehatan}
                    options={{ headerShown: false}}
                />
                <Stack.Screen
                    name = "DetailRiwayatObat" 
                    component={DetailRiwayatObatScreen}
                    options={{ headerShown: false}}
                />
                <Stack.Screen
                    name = "DetailSuratRujukan" 
                    component={DetailSuratRujukanScreen}
                    options={{ headerShown: false}}
                />
                <Stack.Screen
                    name = "ChattingScreen" 
                    component={ChattingScreen}
                    options={{ headerShown: false}}
                />

                <Stack.Screen
                    name = "ChangePassword" 
                    component={ChangePassword}
                    options={{ headerShown: false}}
                /> 
                <Stack.Screen
                    name = "InformasiKesehatanScreen" 
                    component={InformasiKesehatan}
                    options={{ headerShown: false}}
                /> 
            </Stack.Navigator>
        </NavigationContainer>
    )

}

export default Routes