const initPasien = {
    kodePasien: [],
    statusVerifikasiKtp: [],
    informasiKesehatan: [],
}



export const pasienReducer = (state = initPasien, action) => {

    if(action.type === 'SET_KODE_PASIEN')
    {
        return{
            ...state,
            kodePasien: action.value
        }
    }

    if(action.type === 'SET_STATUS_VERIFIKASI_KTP')
    {
        return{
            ...state,
            statusVerifikasiKtp: action.value
        }
    }

    if(action.type === 'SET_INFORMASI_KESEHATAN')
    {
        return{
            ...state,
            informasiKesehatan: action.value
        }
    }

    return state
}