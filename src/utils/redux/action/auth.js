import axios from "axios";
import { Alert } from "react-native";
import { setLoading } from ".";
import { setUser } from "../../AsyncStoreServices";
import { ENDPOINT_API } from "../../httpClient";
import { showMessage } from "../../showMessage";

const API_HOST = {
    url: 'https://puskeslinggarjati.com/api'
}


export const loginAction = (dataLogin, navigation) => (dispatch) =>  {
    axios.post(`${ENDPOINT_API}/dokter/login`, dataLogin)
        .then(res => {
            const token = `${res.data.data.token_type} ${res.data.data.access_token}`;
            const profile_pasien = res.data.data.user
            const id = profile_pasien.id

            setUser({
                token: token,
                id: id,
                name: profile_pasien.name,
                username: profile_pasien.username,
                email: profile_pasien.email,
                device_token: profile_pasien.device_token,
                role: profile_pasien.role,
                authenticated: true
            })
        
            dispatch(setLoading(false))
            
            navigation.reset({index: 0, routes: [{name: 'MainApp'}]});
        }).catch(err => {
            dispatch(setLoading(false))
            showMessage("Email atau password anda salah!")
        })
}
