import * as firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyBrz30dDTafXbi_4EukHSuEMiI34uZhfzw",
    authDomain: "epuskesmas-b4a89.firebaseapp.com",
    databaseURL: "https://epuskesmas-b4a89-default-rtdb.firebaseio.com",
    projectId: "epuskesmas-b4a89",
    storageBucket: "epuskesmas-b4a89.appspot.com",
    messagingSenderId: "402144149957",
    appId: "1:402144149957:web:272c20547d20d02d234bac"
  };

export default !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app()

