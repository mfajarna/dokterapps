import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../components/atoms/Header'
import {fonts} from '../utils/fonts'
import {normalizeFont} from '../utils/normalizeFont'
import FiturComponent from '../components/molecules/FiturComponent'
import { useDispatch, useSelector } from 'react-redux'
import { getUser } from '../utils/AsyncStoreServices'
import Gap from '../components/atoms/Gap'
import axios from 'axios'
import { ENDPOINT_API } from '../utils/httpClient'
import CardPemeriksaan from '../components/molecules/CardPemeriksaan'


const PemeriksaanScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const[data,setData] = useState([]);

  
  const user = async () => {
      const user = await getUser();
      
      const token = user.token;


      var result = await axios.get(`${ENDPOINT_API}/dokter/getPemeriksaan`)
      .then(res => {
          console.log('data', res.data.data)
        setData(res.data.data);
      }).catch(err => {
        console.log(err.message)
      })

    return Promise.resolve(result)
      
  }


  const renderPoli = () => {
    return(
      data.map(item => {
        return(
          <CardPemeriksaan
              key={item.id}
              title={item.pasien.nama_lengkap}
              desc={item.status}
              status={item.status_pemeriksaan}
              no_urut={item.no_urut}
          />
        )
      })
    )
  }

  useEffect(() => {
    user();
    console.log('data', data);
  }, [])


  return (
    <View style={styles.container}>
        <Header
          title="List Pemeriksaan Pasien"
          onBack={() => navigation.reset({index:0, routes:[{name:'MainApp'}] })}
        />
        <View style={styles.content}>
            <Text style={{

              fontSize: normalizeFont(13),
              color: 'black'
            }}>Pasien Pemeriksaan</Text>
            <Gap height={10} />
            {renderPoli()}
        </View>
    </View>
  )
}

export default PemeriksaanScreen

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white'
  },
  content:{
    paddingHorizontal: 20,
    marginTop: 20
  }
})