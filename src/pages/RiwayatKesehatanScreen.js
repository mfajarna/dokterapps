import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header'
import { normalizeFont } from '../utils/normalizeFont'
import Gap from '../components/atoms/Gap'
import CardRiwayatKesehatan from '../components/molecules/CardRiwayatKesehatan'
import { useState } from 'react'
import { getUser } from '../utils/AsyncStoreServices'
import axios from 'axios'
import { ENDPOINT_API } from '../utils/httpClient'
import { useEffect } from 'react'

const RiwayatKesehatanScreen = ({navigation}) => {
  const[data,setData] = useState([])

  const fetchData = async () =>{
      const user = await getUser();
      const id = user.id;

      var result = await axios.get(`${ENDPOINT_API}/dokter/riwayatkesehatanpasien`)
      .then(res => {
        setData(res.data.data);
      }).catch(err => {
        console.log(err.message)
      })

    return Promise.resolve(result)
  }

  useEffect(() => {
      fetchData();
  },[])

  return (
    <View style={styles.container}>
        <Header
          title="Riwayat Kesehatan"
          onBack={() => navigation.reset({index:0, routes:[{name:'MainApp'}] })}
        />
        <ScrollView>
        <View style={styles.content}>
          {data.map(item => {
            var tanggal = item.created_at;
            var parse = new Date(tanggal);
            var tanggal = parse.getDate()
            var bulan = parse.getMonth();
            var tahun = parse.getFullYear();

            var fullTanggal =tanggal+'-'+bulan+'-'+tahun

            return(
              <TouchableOpacity key={item.id} onPress={() => navigation.navigate('DetailRiwayatKesehatan', item.id)} style={{
                backgroundColor: 'white',
                marginBottom: 20,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,

                elevation: 5,
                paddingVertical: 10,
                paddingHorizontal: 15,
                borderRadius: 20,
              }}>
                  <Text>DETAIL PASIEN</Text>
                  <Gap height={10} />
                  <Text>Nama Pasien: {item.nama_lengkap}</Text>
              </TouchableOpacity>
            )
          })}
          

        </View>
        </ScrollView>

    </View>
  )
}

export default RiwayatKesehatanScreen

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white'
  },
  content:{
    paddingHorizontal: 20,
    marginTop: 20
  }
})