import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../components/atoms/Header'
import { getUser } from '../utils/AsyncStoreServices'
import { ENDPOINT_API } from '../utils/httpClient'
import axios from 'axios'
import CardRiwayatKesehatan from '../components/molecules/CardRiwayatKesehatan'
import CardRiwayatObat from '../components/molecules/CardRiwayatObat'
import CardSuratRujukan from '../components/molecules/CardSuratRujukan'

const DetailSuratRujukan = ({navigation, route}) => {
    const params = route.params;
    const[data,setData] = useState([])

    const fetchData = async () =>{
        const user = await getUser();
        const id = user.id;
  
        var result = await axios.get(`${ENDPOINT_API}/suratrujukan/getSuratRujukan?id_pasien=${params}`)
        .then(res => {
          setData(res.data.data);
        }).catch(err => {
          console.log(err.message)
        })
  
      return Promise.resolve(result)
    }
  
    useEffect(() => {
        fetchData();

        console.log('dat', params)
    },[])
  return (
    <View style={styles.container}>
        <Header
          title="Detail Surat Rujukan"
          onBack={() => navigation.goBack()}
        />

        <ScrollView>
            <View style={styles.content}>
        {data.map(item => {
                var tanggal = item.created_at;
                var parse = new Date(tanggal);
                var tanggal = parse.getDate()
                var bulan = parse.getMonth();
                var tahun = parse.getFullYear();
            
                var fullTanggal =tanggal+'-'+bulan+'-'+tahun
                var nameFile = item.name_file;
            return(
                <CardSuratRujukan
                    key={item.id}
                    no_surat={item.no_surat}
                    tanggal={fullTanggal}
                    onPress={() => navigation.navigate('LihatSuratRujukan', nameFile)}
                />
            )
                })}
            </View>
        </ScrollView>
    </View>
  )
}

export default DetailSuratRujukan

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
      },
      content:{
        paddingHorizontal: 20,
        marginTop: 20
      }
})