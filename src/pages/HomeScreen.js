import { Alert, ScrollView, StyleSheet, Text, TouchableOpacity, View, DevSettings } from 'react-native'
import React, { useEffect, useState } from 'react'
import { deleteUser, getUser, setUser } from '../utils/AsyncStoreServices'
import { showToast } from '../utils/showToast'
import Toast from 'react-native-toast-message'
import { color } from '../utils/colors'
import { fonts } from '../utils/fonts'
import { IcNotif, IcUser } from '../assets/icon'
import Gap from '../components/atoms/Gap'
import CustomButton from '../components/molecules/CustomButton'
import FiturComponent from '../components/molecules/FiturComponent'
import { useDispatch, useSelector } from 'react-redux'
import { setLoading } from '../utils/redux/action'
import axios from 'axios'
import { showMessage } from '../utils/showMessage'
import { ENDPOINT_API } from '../utils/httpClient'

const HomeScreen = ({navigation}) => {

  const[name,setName] = useState('');
  const[verifktp,setVerifKtp] = useState(false);
  const dispatch = useDispatch();
  const[isStatusKtp, setIsStatusKtp] = useState('');
  const[statusKtp, setStatusKtp] = useState('');
  const[noAntrian, setNoAntrian] = useState();
  const[statusAntrian, setStatusAntrian] = useState();


  // get data user
  const user = async () => {
        dispatch(setLoading(true))
        const dataUser = await getUser();
    
        setVerifKtp(dataUser.is_verificationktp)
        setName(dataUser.name)

       dispatch(setLoading(false))

       return Promise.resolve(dataUser)
  }



  useEffect(() => {
    dispatch(setLoading(false))
    user();
    
  }, [user])
  



  return (
    <View style={styles.container}>
   
      <View style={styles.topContent}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{justifyContent: 'center',}}>
            <IcUser />
          </View>
          
          
          <View style={{marginRight: 50, marginLeft: 20, marginTop: 10}}>
            <Text style={styles.name}>Hai, {name}</Text>
            <Text style={styles.desc}>Selamat Datang Kembali</Text>
          </View>

        </View>
        
      </View>

      <View style={{
        flex: 5,
        paddingHorizontal: 25,
      }}>
                <View style={styles.contentFitur}>
        <Text style={{
          
          fontSize: 16,
          color: 'black'
        }}>Pelayanan kami</Text>


      <Gap height={19} />
        <FiturComponent 
              onPress={() => navigation.navigate("PemeriksaanScreen")}
              title="Pemeriksaan"
              desc="Pilih pemeriksaan sesuai dengan gejala yang anda
              alami"

            />

      <Gap height={13} />
        <FiturComponent 
              onPress={() => navigation.navigate("RiwayatKesehatanScreen")}
              title="Riwayat Kesehatan"
              desc="Lihat riwayat kesehatan anda sesuai dengan pemeriksaan terakhir"

          />

      <Gap height={13} />
        <FiturComponent 
              onPress={() => navigation.navigate("InformasiKesehatanScreen")}
              title="Informasi Kesehatan"
              desc="Informasi tentang dunia kesehatan"

          />

      <Gap height={13} />
        <FiturComponent 
              onPress={() => navigation.navigate("RiwayatObatScreen")}
              title="Riwayat Obat"
              desc="Riwayat penggunaan obat sesuai dengan
              pemeriksaan"

          />

      <Gap height={13} />
        <FiturComponent 
              onPress={() => navigation.navigate("SuratRujukanScreen")}
              title="Surat Rujukan"
              desc="Lihat surat rujukan untuk puskesmas"

          />

      <Gap height={13} />
        </View>
      </View>
    </View>
  )
}

export default HomeScreen

const styles = StyleSheet.create({
    container:{
      backgroundColor: '#F1F5FD',
      flex: 1
    },
    topContent: {
      flex: 1,
      backgroundColor: color.primary,
      paddingHorizontal: 26,
      paddingTop: 23
    },
    name:{

      color: color.white,
      fontSize: 20
    },
    desc:{
      color: color.white,
      fontSize: 13
    },
    antrianWrapper:{
        paddingVertical: 15,
        marginTop: 40,
        height: 175,
        backgroundColor: 'white',
        borderRadius: 10,

        shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,

      elevation: 10,
    },
    statusAntrian:{
      
      color: 'black',
      fontSize: 16,
      textAlign: 'center'
    },
    descAntrian:{

      color: '#9F9F9F',
      fontSize: 13,
      textAlign: 'center',
      paddingHorizontal: 16
    },
    contentFitur:{
      marginTop: 40,
      paddingHorizontal: 5
    }

})