import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../components/atoms/Header'
import { getUser } from '../utils/AsyncStoreServices'
import { ENDPOINT_API } from '../utils/httpClient'
import axios from 'axios'
import CardRiwayatKesehatan from '../components/molecules/CardRiwayatKesehatan'
import CardRiwayatObat from '../components/molecules/CardRiwayatObat'

const DetailRiwayatObatScreen = ({navigation, route}) => {
    const params = route.params;
    const[data,setData] = useState([])

    const fetchData = async () =>{
        const user = await getUser();
        const id = user.id;
  
        var result = await axios.get(`${ENDPOINT_API}/dokter/riwayatobatpasien?id_pasien=${params}`)
        .then(res => {
          setData(res.data.data);
        }).catch(err => {
          console.log(err.message)
        })
  
      return Promise.resolve(result)
    }
  
    useEffect(() => {
        fetchData();

        console.log('dat', data)
    },[])
  return (
    <View style={styles.container}>
        <Header
          title="Detail Riwayat Obat"
          onBack={() => navigation.goBack()}
        />

        <ScrollView>
            <View style={styles.content}>
        {data.map(item => {

            return(
              <CardRiwayatObat
                key={item.id}
                riwayat_obat={item.resep_obat}
              />
            )
                })}
            </View>
        </ScrollView>
    </View>
  )
}

export default DetailRiwayatObatScreen

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
      },
      content:{
        paddingHorizontal: 20,
        marginTop: 20
      }
})