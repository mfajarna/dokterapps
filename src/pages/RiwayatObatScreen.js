import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header';
import { useState } from 'react';
import { useEffect } from 'react';
import { ENDPOINT_API } from '../utils/httpClient';
import axios from 'axios';
import { getUser } from '../utils/AsyncStoreServices';
import CardRiwayatObat from '../components/molecules/CardRiwayatObat';
import Gap from '../components/atoms/Gap';

const RiwayatObatScreen = ({navigation}) => {
  const[data,setData] = useState([])

  const fetchData = async () =>{
      const user = await getUser();
      const id = user.id;

      var result = await axios.get(`${ENDPOINT_API}/dokter/riwayatobatpasien`)
      .then(res => {
        setData(res.data.data);
      }).catch(err => {
        console.log(err.message)
      })

    return Promise.resolve(result)
  }

  useEffect(() => {
      fetchData();
  },[])
  return (
    <View style={styles.container}>
        <Header
          title="Riwayat Obat"
          onBack={() => navigation.reset({index:0, routes:[{name:'MainApp'}] })}
        />
        <ScrollView>
        <View style={styles.content}>
        {data.map(item => {

            return(
              <TouchableOpacity key={item.id} onPress={() => navigation.navigate('DetailRiwayatObat', item.id)} style={{
                backgroundColor: 'white',
                marginBottom: 20,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,

                elevation: 5,
                paddingVertical: 10,
                paddingHorizontal: 15,
                borderRadius: 20,
              }}>
                  <Text>DETAIL PASIEN</Text>
                  <Gap height={10} />
                  <Text>Nama Pasien: {item.nama_lengkap}</Text>
              </TouchableOpacity>
            )
          })}
        </View>
        </ScrollView>

    </View>
  )
}

export default RiwayatObatScreen

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white'
  },
  content:{
    paddingHorizontal: 20,
    marginTop: 20
  }
})